var searchData=
[
  ['a0_314',['A0',['../namespaceFindPos.html#a09ec1035205bc027c1272d486bdad0fb',1,'FindPos']]],
  ['a1_315',['A1',['../namespaceFindPos.html#a126b36d8402b9c21460fb28ca4f96a3d',1,'FindPos']]],
  ['a6_316',['A6',['../namespaceFindPos.html#a316c18b8c3ad03cd1e97188e47b6cf7c',1,'FindPos']]],
  ['a7_317',['A7',['../namespaceFindPos.html#a819e06f0c698fc1f58780fa6f0fc1f6e',1,'FindPos']]],
  ['adc_318',['adc',['../namespacemain3.html#a68460fccdc69736e5daaef386d57eebc',1,'main3']]],
  ['adc_5fxm_319',['ADC_xm',['../classscanner_1_1Scan.html#a0381b90e8a14e901b96f01c1a0129352',1,'scanner::Scan']]],
  ['adc_5fym_320',['ADC_ym',['../classscanner_1_1Scan.html#a745fecda9ab7c12b5d5f6b4b10313342',1,'scanner::Scan']]],
  ['addr_321',['addr',['../namespacemain4.html#a61020df103d1830fffbf96076e8ba201',1,'main4']]],
  ['ambtemp_322',['ambtemp',['../namespaceplotter.html#a91aefb1f731ad3d0434afb85a6745b84',1,'plotter']]],
  ['angle_323',['angle',['../classencoder_1_1EncoderDriver.html#aeefeec289b3171140dd2118df3b30f4e',1,'encoder.EncoderDriver.angle()'],['../namespaceshares.html#a2624eb59e51a8f740b1c36b3bde9545c',1,'shares.angle()']]],
  ['aveg_5ftim_324',['aveg_tim',['../namespacelab2main.html#a959c6a54b4f7066319fa2ffe980eb176',1,'lab2main']]],
  ['average_5ftime_325',['average_time',['../namespacelab2main.html#a833b52359e18d748a9e7b377cb1f91a6',1,'lab2main']]]
];
