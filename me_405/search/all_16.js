var searchData=
[
  ['x_5fcenter_211',['x_center',['../classscanner_1_1Scan.html#a9092399578bf294a0b02b13c98c106f9',1,'scanner.Scan.x_center()'],['../namespacescanner.html#a308a67a244c4ea3d44b181751534685e',1,'scanner.x_center()']]],
  ['x_5fconv_212',['x_conv',['../classscanner_1_1Scan.html#a64ff94052221d2a61e45ec03f7c88152',1,'scanner::Scan']]],
  ['x_5fcounts_213',['x_counts',['../namespaceUI__front.html#a4f174e3bc0932ee762058735bb2886bc',1,'UI_front']]],
  ['xlength_214',['xlength',['../classFindPos_1_1TouchControl.html#a42f5030a9dcfaeebc7401f223725897e',1,'FindPos::TouchControl']]],
  ['xloc_215',['xloc',['../classscanner_1_1Scan.html#af1b1e8aec13a0442f969885251407f78',1,'scanner::Scan']]],
  ['xm_216',['xm',['../classFindPos_1_1TouchControl.html#a1d9671d2086b940e478c3b9883afc1ce',1,'FindPos.TouchControl.xm()'],['../classscanner_1_1Scan.html#a0f2affb2f37d3fd7d3cf9a109895caed',1,'scanner.Scan.xm()']]],
  ['xp_217',['xp',['../classFindPos_1_1TouchControl.html#add0ab400af5e68b4fb30970d558ddd85',1,'FindPos.TouchControl.xp()'],['../classscanner_1_1Scan.html#a56523d807f2b8bbe8418b9379669f3b3',1,'scanner.Scan.xp()']]],
  ['xpos_218',['xpos',['../classscanner_1_1Scan.html#aca7774ae677497c0c1c774f94e866cfa',1,'scanner::Scan']]],
  ['xscan_219',['xscan',['../classFindPos_1_1TouchControl.html#addd7ba0bf49ac18af47b2d1b9f0cf0da',1,'FindPos::TouchControl']]]
];
