var searchData=
[
  ['data_37',['data',['../classscanner_1_1Scan.html#a6de1896bce11eb0bdcff8f81d980fcf5',1,'scanner.Scan.data()'],['../namespaceUI__front.html#a6fd3153bbe3baed376cd2dcdad579244',1,'UI_front.data()']]],
  ['data_5flist_38',['data_list',['../namespaceUI__front.html#af341c3e2a1e6ee49c036a993e7a82aa5',1,'UI_front']]],
  ['databytes_39',['databytes',['../namespacemcp9808.html#a45470713280e8c98a37244745aa244f0',1,'mcp9808']]],
  ['delta_40',['delta',['../classencoder_1_1EncoderDriver.html#a99e9351c942f7909677e28d8bef15679',1,'encoder.EncoderDriver.delta()'],['../namespacelab2main.html#a75efc8cba1807262bfed89cdc9732e84',1,'lab2main.delta()']]],
  ['dimes_41',['dimes',['../namespaceVendotron.html#afd4a05949f8e000702c63fee9db15b8e',1,'Vendotron']]],
  ['disable_42',['disable',['../classMotorDriver-M_1_1MotorDriver.html#aed47d0f271228dd8fffec95530fa1d23',1,'MotorDriver-M.MotorDriver.disable()'],['../classMotorDriver_1_1MotorDriver.html#abb9a67928c8ed29dd06b04727813411a',1,'MotorDriver.MotorDriver.disable()']]],
  ['duty_43',['duty',['../classMotorDriver-M_1_1MotorDriver.html#a021146a3089ff9f4d2bece80a4f858dd',1,'MotorDriver-M.MotorDriver.duty()'],['../classMotorDriver_1_1MotorDriver.html#ac5b1e7813ea1d7fd4a71ca24d5515f36',1,'MotorDriver.MotorDriver.duty()']]]
];
