var namespaces_dup =
[
    [ "OpenLoopModel", "page2.html#OpenLoopModel", null ],
    [ "ClodeLoopModel", "page2.html#ClodeLoopModel", null ],
    [ "Analysis", "namespaceAnalysis.html", null ],
    [ "coretemp", "namespacecoretemp.html", null ],
    [ "cotask", "namespacecotask.html", null ],
    [ "encoder", "namespaceencoder.html", null ],
    [ "FindPos", "namespaceFindPos.html", null ],
    [ "lab2main", "namespacelab2main.html", null ],
    [ "main3", "namespacemain3.html", null ],
    [ "main4", "namespacemain4.html", null ],
    [ "main9", "namespacemain9.html", null ],
    [ "mainpage", "namespacemainpage.html", null ],
    [ "mcp9808", "namespacemcp9808.html", null ],
    [ "MotorDriver", "namespaceMotorDriver.html", null ],
    [ "MotorDriver-M", "namespaceMotorDriver-M.html", null ],
    [ "plotter", "namespaceplotter.html", null ],
    [ "scanner", "namespacescanner.html", null ],
    [ "shares", "namespaceshares.html", null ],
    [ "task_share", "namespacetask__share.html", null ],
    [ "UI_front", "namespaceUI__front.html", null ],
    [ "Vendotron", "namespaceVendotron.html", null ]
];