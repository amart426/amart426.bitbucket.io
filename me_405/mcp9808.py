'''
@file mcp9808.py

\brief      Sets up the I2C interface for the MCP9808 temperature sensor and MCU comunication.
@details    The MCP9808 temperature sensor uses an I2C interface to communicate 
            with the STM32 microcontroller. For the MCP9808 and STM32 board physical
            connection refer to mainpage documentation. The function init() is
            use to set up the I2C interface comunication. First step is to
            to create the bus. Next step is to specify the address for the the 
            MCP9808. This information can be access from the MCP9808 documentation.
            This function check() has the purpose of scaning for slaves on the 
            bus and comparing the the scan address with specified address.
            The celsius() and fahrenheit() functions read the bytes
            from the memory starting with the address specified. The read
            value is saved to an array and converted to a temperature value. 

@author Matthew Pfeiffer
@author Adan Martinez

@date Feb. 8, 2021
'''

def init(sensor, addr):
    '''
    @brief            A function that initializes the I2C interface. 
    @details          This function takes two paramenters, the sensor bus and
                      address number. Then it sets up the I2C for the MCP9808 and
                      MCU interface.
    @param sensor     Specifies the I2C bus. This project uses bus 1.
    @param addr       A parameter that specifies the address for the MCP9808
                      sensor. This information can be access from the MCP9808
                      documentation.         
    '''
    ## Importing modules
    import pyb
    import array
    from pyb import I2C
    import utime
    
    global address     ## Creating global parameters to cache the address value and i2c bus.
    global i2c
    
    i2c = sensor                            ## Specifies to which bus to use. This value is set in the main.py script. 
    i2c.init(I2C.MASTER,baudrate=115273)    ## Initializes the bus and baudrate. 
    address = addr                          ## Takes the specified address of the MCP9808 sensor. 
    
def check():
    '''
    @brief            A function that scans for slaves on the bus.
    @details          Scans all I2C addresses from 0x01 to 0x7f and returns a list 
                      of those that respond. If the address return equals the 
                      address specified, then it returns a True value.
    '''
    validaddr = i2c.scan()         ## This method scans the slave bus and returns a value if any. 
    if validaddr[0] == address:    ## Checks that the i2c interface can detect the mcp9808 by comparing the read
        return(True)                # to the specified address. Returns a true value if the addresses are the same.    

def celsius():
    '''
    @brief            A function that returns the ambient temperature in celsius.
    @details          This function first creates an array to save data. It then
                      uses the methond i2c.mem_read(bytes from memory, slave 
                      address, starting bytes) to get the temperature reading
                      value back. The byte value return is then sort out and
                      converted to a temperature value in degree celsius.
    '''
    import array    ## Importing the array module
    databytes = array.array('B',[0 for index in range(2)])    ## Creating an array that specify the bytes to reads. In this case 3 bytes.
    i2c.mem_read(databytes,address,5)     ## Calling the i2c.mem_read() method, and specifying to read 3 bytes from memory for the address 
                                           # given and the starting address (5) in the bus.
    MSB = databytes[0]&31                 ## Takes the most significant bytes (which is the first and 30) from the array.
    LSB = databytes[1]                    ## Takes the least significant byte from the array.
    if MSB&16 == 16:
        temp = 256 - (MSB*16 + LSB/16)    ## Using the twos complements to get temperature in celsius.
        return temp
    else:
        temp = MSB*16 + LSB/16
        return temp
        
def fahrenheit():
    '''
    @brief            A function that returns the ambient temperature in fahrenheit.
    @details          This function first creates an array to save data. It also
                      uses the i2c.mem_read(bytes from memory, slave 
                      address, starting bytes) method to get the temperature reading
                      value back. The return value is then sort out and
                      convert to a temperature value in degree fahrenheit.
    '''
    import array
    databytes = array.array('B',[0 for index in range(2)])   ## An array to save the first 3 byte readings.
    i2c.mem_read(databytes,0x18,5)      ## Calling the i2c.mem_read() method, and specifying to read 3 bytes 
                                         # from memory of the address 0x18, starting at address 5 
    MSB = databytes[0]&31               ## Takes the most significant bytes (which is the first and 30) from the array.
    LSB = databytes[1]                  ## Takes the least significant byte from the array.
    if MSB&16 == 16:
        temp = (256 - (MSB*16 + LSB/16))*(9/5)+32     ## Converts the read value to a fahrenheit temperature value.
        return temp
    else:
        temp = (MSB*16 + LSB/16)*(9/5)+32            ## Converts the read value to a fahrenheit temperature value.
        return temp

if __name__ == "__main__":
    '''
    @brief            Only runs when this script is executed as a standalone. 
    @details          This portion of the program only runs when this program
                      runs independently from other program. Will not run if
                      the mcp9808 module is imported. Its main purpose is for 
                      user testing only. 
    '''
    i2c = pyb.I2C(1)       ## Specifying bus to for comunication.
    databytes = array.array('B',[0 for index in range(2)])    ## Creating array for 3 readings. 
    while True:
        try: 
            print(str(celsius()) + ' [C]')
            print(str(fahrenheit()) + ' [F]')
            utime.sleep(1)
        except KeyboardInterrupt:
            print('Program Ended By User')
            break
