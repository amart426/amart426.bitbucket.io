'''
@file coretemp.py

@brief      Reads and returns the Microcontroller's core temperature. 
@details    This file contains one function that has the purpose of measuring
            the MCU core temperature. This is perform using the ADCAll()
            method from pyb. The STM32 board uses a 12bit resolution for
            the temperature. Using the proper conversion, the ADC read value
            is converted to degrees fahrenheit.

@author Matthew Pfeiffer
@author Adan Martinez-Cruz

@date 02/04/2021
'''
## Importing modules
import pyb
import utime

def readtemp():
    '''
    @brief            A function that reads the MCU core temperature. 
    @details          This function sets up the pyb.ADCAll() method which is
                      use to read the temperature from the MCU core. For
                      conversion purposes, the adc.read_core_vref() method
                      is needed to convert the adc value to a temperature.
                      Once the math is finish, the function returns the 
                      temperature in degrees fahrenheit. 
    '''
    adcall = pyb.ADCAll(12,0x10000)    ## Settinng up the pyb.ADCAll() method with a 12bit resolution.
    coretemp = (adcall.read_core_temp()/adcall.read_core_vref())*(9/5)+32  ## Converting the ADC value to degrees fahrenheit.
    return(coretemp)  ## Returning the core temperature [F]


if __name__ == "__main__":
    '''
    @brief            Only runs when this script is executed as a standalone. 
    @details          This portion of the program only runs when this program
                      runs independently from other program. Will not run if
                      the if imported. Its main purpose is for user testing 
                      only. 
    '''
    while True: 
        try:
            print(coretemp())  ## Calls and prints the core temperature [F]
            utime.sleep(1)     ## Delays the code executing for 1 second. 
        except KeyboardInterrupt:  ## Ends the execution when user presses CTRL-C
            break
