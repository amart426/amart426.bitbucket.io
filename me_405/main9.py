#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main9.py

@brief    This file uses the cotask.py and task_share.py files to create 
          a scheduler for the ball-balancing controller.
@details  The main purpose of this code is use the FindPos, MotorDriver, and encoder
          drivers to create a controller for balacing a ball on a platform. The 
          platform is composed of two separate motors and encoder. The goal is
          to create a method for obtaining the ball state at any given time. 
          Another method that is in charge of obtaining the platforms state. 
          And a last method which has the duty of commanding the motors motions
          relative to the motion of the ball. Please read each individual method
          for detail function. This main file also works along with cotask.py and
          task_share.py which are imported from JR Ridgely BitBucket sourcecode.
          
@author Matthew Pfeiffer
@author Adan Martinez
@date Mar. 15, 2021
"""

import pyb
from micropython import const, alloc_emergency_exception_buf
import gc

import cotask
import task_share

import FindPos
import MotorDriver
import encoder

alloc_emergency_exception_buf (100)

def Ball_state_fun ():
    '''
    @brief      The Ball_state_fun function is responsible for obtaining the ball position
    @details    Ball_state_fun function uses the FindPos.py driver to create a driver for 
                the touchscreen on to of the platform. This touchscreen is then scanned
                in the x, y, and z axes all at once and the value is placed in a queue
                used to calculate the torque input necessary.
    '''
    state = 0
   
    while True:
        if state == 0:
            ## Runs the initial state
            
            ## Sets up the TocuhControl from the FindPos module.
            TouchScreen = FindPos.TouchControl(pyb.Pin.cpu.A6, pyb.Pin.cpu.A0, 
                                               pyb.Pin.cpu.A7, pyb.Pin.cpu.A1,
                                               176, 100, (176/2, 100/2))
            state = 1  # Sets the next state.

        elif state == 1:
            ## Runs state 1
            
            if not Ball_Info.full ():
                BallPos = TouchScreen.scanall()
                Ball_Info.put(BallPos[0])
                Ball_Info.put(BallPos[1])
                Ball_Info.put(BallPos[2])
            else:
                #print('ball info full')
                pass
        yield (state)

def Platf_state_fun ():  
    '''
    @brief       A method that check the state of the platform. 
    @details     This method uses the EncoderDriver class to check the state of
                 the platform. The way the encoder are set up are use timer 4
                 and 8 to reach from two separate encoders. The method is consists
                 of two states. First, it sets up the EncoderDrivers. Next state,
                 it gets the angle and speed from both encoders and saves them
                 into the task_share file. 
    '''
    ## Initializes the state.
    state = 0
    while True:
        if state == 0:
            ## Runs the first state.
            # Sets up the first encoder with timer 4, pin B6, pin B7, and a 10 second interval.
            ENCy = encoder.EncoderDriver( 4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7,10)
            # Sets up the second encoder with timer 8, pin C6, pin C7, and a 10 second interval.
            ENCx = encoder.EncoderDriver( 8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7,10)
            state = 1  # Next state to run.
        elif state == 1:
            ## Runs the second state.
            
            ## Only runs if Pos_Info is not full.
            if not Pos_Info.full ():
                ## Get the angle and speed reading from both encoders and saves it 
                #  into Pos_Info
                Pos_Info.put((50/60)*ENCy.get_angle())
                Pos_Info.put((50/60)*ENCx.get_angle ())
            ## Only runs if Speed_Info is not full.   
            if not Speed_Info.full ():
                ## Gets the speed from the encoders and saves it to the Speed_Info.
                Speed_Info.put((50/60)*ENCy.get_speed ())
                Speed_Info.put((50/60)*ENCx.get_speed ())
                
        yield (state)
            
def Motor_fun ():
    '''
    @brief     A method that comands the motion of the motor depending on the 
               platform and ball state.
    @details   This method consist of a FSM running in a while loop. The initial 
               state is initialize the pins for the motors control. The gain 
               values  (k1, k2, k3, k4) need to be specify here. The next state 
               had the functionality to retrieve the data save in the task_share. 
               If there is not data yet, the program will continue running in a 
               while loop. In state 2, the program calculate the torque needed
               to balance the system. It then calculates and sets the necessary 
               duty cycle. It then enables both the motors and runs them. Then,
               it goes back to state 1 and continues the cycle.
    '''
    ## Initial state
    state = 0
    while True:
        if state == 0:
            ## Runs state 0
            ## Create the pin objects used for interfacing with the motor driver
            pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)   
            pin_nFAULT  = pyb.Pin(pyb.Pin.board.PB2)
            pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4) 
            pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
            pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)
            pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1) 
            
            ## Sets up and initializes the nFault pin as B15
            nfaultset = pyb.Pin(pyb.Pin.cpu.B15)
            nfaultset.init(mode=pyb.Pin.OUT_PP, value=0)
            
            ## Sets value a channel for the motor.
            ch1 = 1
            ch2 = 2
            ch3 = 3
            ch4 = 4
            
            # Create the timer object used for PWM generation
            timer = pyb.Timer(3, freq=20000)
            # Sets up MotorDriver class with the appropiate inputs.
            moex = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN1, pin_IN2, ch1, ch2, timer)
            moey = MotorDriver.MotorDriver(pin_nSLEEP, pin_nFAULT, pin_IN3, pin_IN4, ch3, ch4, timer)
            ## Sets the value of the controller gains
            k1 = -3.1829
            k2 = -0.2771
            k3 = -9.0905
            k4 = -3.9995
            ## Sets the inital positions to zero.
            good_x = 0
            good_y = 0
            R = 2.21
            Vdc = 12
            Kt = 13.8
            
            state = 1  ## Next state to run.
    
        elif state == 1:
            ## Runs state 1
    
            if Ball_Info.any ():
                ## Runs only if Ball_Info has content.
                x_pos = Ball_Info.get()  # Gets the x position from the Ball_Info data
                y_pos = Ball_Info.get()  # Gets the y position from the Ball_Info data
                z_pos = Ball_Info.get()  # Gets the z position from the Ball_Info data
                if int(z_pos) == 0:
                    ## Only runs if the ball is not touching the touch panel.
                    if good_x != False:
                        # Only runs if the x position is not equal to False.
                        x_pos_former = good_x  # Sets the good_x to equal the x_pos_former 
                        good_x = x_pos   # Sets the good_x value equal to the X position.
                        x_speed = (good_x-x_pos_former)/(2*(1e-3))  # Calculates the speed of change of the x position.
                    else:
                        good_x = x_pos
                        x_speed = 0
                        
                    if good_y != False:
                        ## Check that the value from the y position and calculates the 
                        #  speed of change of the y position. 
                        y_pos_former = good_y
                        good_y = y_pos
                        y_speed = (good_y-y_pos_former)/(2*(1e-3))
                    else:
                        good_y = y_pos
                        y_speed = 0
                else:
                    ## Only runs if the position of the ball is at the center of the platform.
                    x_speed = 0
                    y_speed = 0
                    good_x = False
                    good_y = False                   
            if Pos_Info.any ():
                ## Only runs when the Pos_Info has content. 
                theta_y = Pos_Info.get () # Retrieves the theta_y from Pos_Info
                theta_x = Pos_Info.get () # Retrieves the theta_x from Pos_Info
            if Speed_Info.any ():
                ## Retrieves the angular and linear acceleration data from Speed_Info.
                theta_dot_y = Speed_Info.get ()
                theta_dot_x = Speed_Info.get ()
            else:
                ## Only runs when there is no motion. 
                theta_y = 0
                theta_x = 0
                theta_dot_y = 0
                theta_dot_x = 0
            state = 2 # Sets the next state to run
        elif state == 2:
            ## Runs state 2
            # Sets the speed in the y and x direction to 0. 
            x_speed = 0
            y_speed = 0
            ## Sets the torque value using a tuple with the gains initially stated.
            Torque_x = (-k3*good_y - k4*theta_y -k1*y_speed - k2*theta_dot_y) 
            Torque_y = (-k3*good_x - k4*theta_x -k1*x_speed - k2*theta_dot_x)
            ## Sets the duty cycle of the motor on the torque input, voltage, and Kt.
            Dutyx = -(R*100/(Vdc*Kt))*Torque_x
            Dutyy = -(R*100/(Vdc*Kt))*Torque_y  
            ## Enables the motor 1
            moex.enable()            
            moex.set_duty(Dutyx) # Sets the duty cycle
            print(Dutyx)
            moey.enable()  #Enables motor 2
            moey.set_duty(Dutyy)  #Sets the duty cycle for motor two.
            
            state = 1 ## Sets the next state to run     
        yield(state)
        
# ----------------------------------------------------------------------

if __name__ == "__main__":
    
    print ('\033[2JTesting the scheduler for controller\n')
    
    Ball_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                  name = "Ball_Pos")
    Pos_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                    name = "Platform_Pos")
    Speed_Info = task_share.Queue ('f', 10, thread_protect = False, overwrite = False,
                                    name = "Platform_Pos")
    
    #BallPos = cotask.Task (Ball_state_fun, name = "Ball_Status", priority = 2, period = 1.5, profile = True, trace = False)
    PlatPos = cotask.Task (Platf_state_fun, name = "Platform_Status", priority = 1, period = 3, profile = True, trace = False)
    MotorCont = cotask.Task (Motor_fun, name = "Motor_Control", priority = 3, period = 5, profile = True, trace = False)
    
    #cotask.task_list.append (BallPos)
    cotask.task_list.append (PlatPos)
    cotask.task_list.append (MotorCont)
    
    gc.collect()
    
    vcp = pyb.USB_VCP ()
    while not vcp.any ():
        cotask.task_list.pri_sched ()
        
    vcp.read ()
    
    # Print a table of task data and a table of shared information data
    print ('\n' + str (cotask.task_list) + '\n')
    print (task_share.show_all ())
    print (BallPos.get_trace ())
    print ('\r\n')
    
    
    
    
        
