#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file main.py

This file cosists of the main file that only run in Nucleo Board. The file consist
of various objects from other classes. This file also serves a port for sending 
Kp value to our ClosedLoop driver and the time interval for running our controller. 

Created on Mon Nov 23 18:55:46 2020

@author: adanmartinez
"""
import pyb    
from encoder import EncoderDriver
from Motor import MotorDriver
from CL import ClosedLoop
from Controller import Controller
import shares
from UI_back import Task_User

## Setting Up Motor 1 & 2
# Setting up Pin locations
pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15, pyb.Pin.OUT_PP)
pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4) 
pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
pin_IN3     = pyb.Pin(pyb.Pin.cpu.B0)
pin_IN4     = pyb.Pin(pyb.Pin.cpu.B1)
# Setting up timer.
timer = pyb.Timer(3, freq=20000)
# Setting up channels for motor
ch1 = 1
ch2 = 2
ch3 = 3
ch4 = 4
# Creatinng Motor Driver objects
MOT1 = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, ch1, ch2, timer)
MOT2 = MotorDriver(pin_nSLEEP, pin_IN3, pin_IN4, ch3, ch4, timer)

## Creating Encoder Objects
ENC1 = EncoderDriver( 4, pyb.Pin.cpu.B6, pyb.Pin.cpu.B7)
ENC2 = EncoderDriver( 8, pyb.Pin.cpu.C6, pyb.Pin.cpu.C7)

## Setting Up parameters.
# Omega measure by encoder 1
ome_meas1 = ENC1.get_position()
# omega measured by encoder 2
ome_meas2 = ENC2.get_position()

## Setting up ClosedLoop parameters. 
#  ClosedLoop(Kp, omega reference, omega measure)
#  User can change its derised rotational [rad/s] speed here. 
CL = ClosedLoop(shares.Kp, 30, ome_meas1)
shares.interval = 1

task1 = Task_User()
CONTROL = Controller(shares.interval)

while True:
    ## Run back end first and returns Kp value
    task1.run()
    CONTROL.run()
