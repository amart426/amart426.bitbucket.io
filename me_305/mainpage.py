## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @date Nov. 26, 2020
#
#  @section sec_intro Introduction
#  The purpose behind this project is to create first order response plots
#  from motor speed. The project consist of a motor driver class that
#  when prompted it can enable and disable the motors, as well as setting the
#  motors to a desired duty cycle using PWM. The project also has an Encoder
#  Driver which reads or resets the motor position using pyb.Timer. Lastly, the
#  project also includes a ClosedLoop driver which when call can calculate the
#  actuation response and set or get the proportional gain value.
#  Comments: For this lab, my ME305 board stop working and I was having issues
#  connecting to the board. This has been mainly an issue for Mac Users. This made
#  us underperform. 
#
#  @section Notes
#  I had several obstacles that did not allow me to produce plots. First, my
#  PCB board would sometime not connect with my MacBook. My computer was also
#  not working with ampy. Whenever I tried to run my main.py code 
#  For this reason I was not able to test my cooperative scripts.
#  I know that my Motor.py, encoder.py, and ClosedLoop CL.py work fine because
#  I have test them individually. If my code was to run sucessfully, by choosing
#  a good Kp value we can reduce the steady state error between our measured
#  and actual rotational velocity.
#
#
#  @section Respository
#  @ref BitBucket Respository:
#       https://bitbucket.org/amart426/me305_labs/src/master/Lab6/ and
#       https://amart426@bitbucket.org/amart426/amart426.bitbucket.io.git 
# 
#  @section Task Diagram
#  @image html LAB6.png
#
#

