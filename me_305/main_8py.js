var main_8py =
[
    [ "ch1", "main_8py.html#aa0d92a8184196ffe1022c05a518e6df7", null ],
    [ "ch2", "main_8py.html#a1b83156e1532940ea3b40bfd3b14f981", null ],
    [ "ch3", "main_8py.html#a68ebb90386fa3fb81ef8b2a23c1a0d4a", null ],
    [ "ch4", "main_8py.html#a02f67e56867aae4c6b7c446e2dcffd76", null ],
    [ "CL", "main_8py.html#a5ef910cb78561151726cb2a75e94230d", null ],
    [ "CONTROL", "main_8py.html#aade1b1e12d5f5dfcbad0d20e79e50fb6", null ],
    [ "ENC1", "main_8py.html#aab5c7095975a4a61d864fc7509ca8bbf", null ],
    [ "ENC2", "main_8py.html#a1210f3cb932d9e73c75066d4d6bdf458", null ],
    [ "interval", "main_8py.html#a3648c8ba6030446db9221c2e18452af9", null ],
    [ "MOT1", "main_8py.html#af73b261e8d5c22c099c6e52f81253ab5", null ],
    [ "MOT2", "main_8py.html#a2e0f69153b558697bcef312cd5361874", null ],
    [ "ome_meas1", "main_8py.html#a5809801216151b1a371f9f82ca544460", null ],
    [ "ome_meas2", "main_8py.html#a76b157cb44e39174ce19c434042b2644", null ],
    [ "pin_IN1", "main_8py.html#a537d3d4da75a61c087ac80386962f99e", null ],
    [ "pin_IN2", "main_8py.html#afb0a14e68f5dcbaa02d2d01bcd5bc447", null ],
    [ "pin_IN3", "main_8py.html#a846daa136c231f477e196ec37c1432b3", null ],
    [ "pin_IN4", "main_8py.html#aecee7787e1bc597d3d6cd49eeb97133d", null ],
    [ "pin_nSLEEP", "main_8py.html#a1d4462f4e165ffffe2b6ad62c9e4e70b", null ],
    [ "task1", "main_8py.html#af4b8f4290f8d32e70654f6deb864787f", null ],
    [ "timer", "main_8py.html#a2f204a92bc76ebd5b85adc77f48747d6", null ]
];