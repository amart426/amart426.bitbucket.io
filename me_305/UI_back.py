#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

@file UI_back.py

The purpose of this file is used to receive the Kp from the user and returns that value.

Created on Wed Dec  2 00:16:30 2020

@author: adanmartinez
"""
import utime
from pyb import UART
import pyb    
import shares

# serial = pyb.USB_VCP()
# serial.usb.init(flow=pyb.USB_VCP.RTS)
# serial.USB_VCP.isconnected()
uart = UART(2)

class Task_User():
    '''
    @brief      A finite state machine which main goal is to interface with user.
    @details    This class implements a finite state machine to interface with user 
                designed specifically to run in Micropython.
    '''
    ## Constant defining State 0 - Initialization
    S0_INIT      = 0    
    ## Constant defining State 1
    S1_CHECKING  = 1     
    def __init__(self, interval):
        '''
        @brief            Creates a user_interface object.
        @param interval   Representing the elapsed time count(microsecond) 
                          between user state interface. Please leave intact.
        '''
        
        ## The state to run on the next iteration of the task.
        self.state = self.S0_INIT
        
        ##  The amount of time in seconds between runs of the task
        self.interval = int(interval*(1e6))
        
        ## The timestamp for the first iteration 
        self.start_time = utime.ticks_us()
        ## The "timestamp" for when the task should run next
        self.next_time = utime.ticks_add(self.start_time, self.interval)
        
        ## Sets up UART baud rate and UART(3)
        # self.uart = UART(3, 9600)
        # self.uart.init(9600, bits=8, parity=None, stop=1)
        
        ## Setups the pin for LED control
        self.pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        
        # Initial value of Kp
        self.Kp = 0

    def run(self):
        '''
        @brief      Runs one iteration of the task
        '''
        ## Updating current timestamp
        self.curr_time = utime.ticks_us()
        
        ## State 0
        if(self.state == self.S0_INIT):
            print('Please Enter a Kp value: ')
            self.transitionTo(self.S1_CHECKING) #Transition
            
        ## State 1
        elif(self.state == self.S1_CHECKING):
            
            ##First, checks for any characters entered and validates the input.
            if self.uart.any() != 0:
                print('Validating')
                ## reads and returns the input as a byte type and saves it as val.
                self.Kp = self.uart.readline()
                shares.Kp = self.Kp
                    
            else:
                ## Handling non inputs
                print('Please enter a Kp value: ')
                self.transitionTo(self.S1_CHECKING)
        
        
        # Specifying the next time the task will run
        self.next_time += utime.ticks_add(self.next_time, self.interval)
        ## returns the value of Kp
        shares.Kp = self.Kp
        return shares.Kp
    
    def transitionTo(self, newState):
        '''
        @brief      Updates the variable defining the next state to run
        '''
        self.state = newState