var searchData=
[
  ['get_5fangle_27',['get_angle',['../classencoder_1_1_encoder_driver.html#a5129e10752c033c8d5b820226c38e104',1,'encoder::EncoderDriver']]],
  ['get_5fdelta_28',['get_delta',['../classencoder_1_1_encoder_driver.html#ac367cfb053946a822bc70c4129f9fa2a',1,'encoder::EncoderDriver']]],
  ['get_5fkp_29',['get_Kp',['../class_c_l_1_1_closed_loop.html#a1ab2850ae8c489cabd51ea5372ac32e1',1,'CL::ClosedLoop']]],
  ['get_5fposition_30',['get_position',['../classencoder_1_1_encoder_driver.html#a562e77151e296465959df10f63af26f8',1,'encoder::EncoderDriver']]],
  ['get_5fspeed_31',['get_speed',['../classencoder_1_1_encoder_driver.html#a2e2b9182448c8b9552dc1fba8a391d53',1,'encoder::EncoderDriver']]]
];
