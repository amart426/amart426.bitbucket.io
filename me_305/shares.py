#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file     shares.py

@brief    This file containts arrays that are use interchangeably across
          the tasks.
          
Created on Thu Nov  1 01:27:03 2020

@author: adanmartinez
"""
import numpy as np


Kp = None
Speed = np.array([0])
time = np.array([0])

## Setting up interval. By default one numeric value is has units of a second.
interval = None

## Importing data

#NewData = np.genfromtxt('data.txt', delimiter=',')
