#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@file UI_front.py

The purpose of this code is to only run in Python. It uses serial port to send
and receive the motors speed and plots the first order response. 

Created on Mon Nov 23 18:55:05 2020

@author: adanmartinez
"""
import shares
import serial
import matplotlib.pyplot as plt
import numpy as np

## Connects to port. Change port name if different. THe baudrate must match
#  that of putty/iTerm.
#ser = serial.Serial(port='/dev/tty.usbmodem2051339257522',baudrate=115200,timeout=(1))
ser = serial.Serial(port='/dev/tty.usbmodem14503',baudrate=115200,timeout=(1))


def sendChar():
    Kp = input('Enter value for Kp: ')
    ser.write(int((Kp).encode('ascii')))
    Kp_val = ser.readline().decode('ascii')
    return Kp_val

while True:
    sendChar()
    
    ## Create a plot.
    plt.plot(shares.time, shares.speed, 'r')
    plt.xlabel('Time [s]')
    plt.ylabe('Speed. [rad/s]')
    plt.grid(True)
    plt.show()
    
    ## Save data to a .csv file.
    np.savetxt('Lab6.csv', shares.speed, shares.time ,delimiters=',')
    
ser.close()