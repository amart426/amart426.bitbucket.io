var class_motor_1_1_motor_driver =
[
    [ "__init__", "class_motor_1_1_motor_driver.html#a024a0a2c922faee6432056d4a8604816", null ],
    [ "disable", "class_motor_1_1_motor_driver.html#a9ad4f746ef0e7c217ce790f7ab9260b3", null ],
    [ "enable", "class_motor_1_1_motor_driver.html#a794de1aa1bfc8ff660c75bf7f4ec8038", null ],
    [ "set_duty", "class_motor_1_1_motor_driver.html#a420e347ab73de4a9851a5da3434ccfbb", null ],
    [ "channelA", "class_motor_1_1_motor_driver.html#ae06f0ed23bcad40f6ceea593b554df1f", null ],
    [ "channelB", "class_motor_1_1_motor_driver.html#abce6b896fd591528bd641518563fca2e", null ],
    [ "duty", "class_motor_1_1_motor_driver.html#a5944c862c6ffcc5fc6e59c74e17f4bf8", null ],
    [ "IN1_pin", "class_motor_1_1_motor_driver.html#a336e963c802260d78feda5ee6240af89", null ],
    [ "IN2_pin", "class_motor_1_1_motor_driver.html#ab381ffdf92b5d73b017aa0e76a593cc7", null ],
    [ "nSLEEP_pin", "class_motor_1_1_motor_driver.html#a908bf6cd362eb5d56525c5d61551bd9f", null ],
    [ "timchA", "class_motor_1_1_motor_driver.html#a87409c87c3dc21ce1d7d1d8ed9a36af3", null ],
    [ "timchB", "class_motor_1_1_motor_driver.html#afb46d42939aa9695cce0965f2445cc31", null ],
    [ "timer", "class_motor_1_1_motor_driver.html#a93dd3f6608ad30e13b49d9ac609b919d", null ]
];