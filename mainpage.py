## @file mainpage.py
#  Brief doc for mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @package mainpage
#  Brief doc for setting up main page.
#
#  @mainpage
#
#  @author Adan Martinez
#
#  @section sec_305 Portfolio Details
#  The following represents my portofio for ME305 and ME405. See individual modules for details.
#
#  @section me_305 ME 305 Documentation
#  * https://amart426.bitbucket.io/me_305/
#
#  @section me_405 ME 405 Documentation
#  * https://amart426.bitbucket.io/me_405/
#
#  @date Jan. 22, 2021