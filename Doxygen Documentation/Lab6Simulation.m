%% Lab 6: Platform and Ball Simulation
% Adan Martinez
% This file consist of a program that simulates the step response of state
% space vector with given initial conditions and input time step. The 
% result is save in a plot. The following code walksthrough the process
% from linearizing to plot our state space function. 

%% Clearing Workspace
clear all

%% Constants
r_m = 60; %mm
l_r = 50; %mm
r_B = 10.5; %mm
r_G = 42; %mm 
l_P = 110; %mm
r_P = 32.5; %mm
r_C = 50; %mm
m_B = 30; %g
m_P = 400; %g
I_P = 1.88*10^6; % [g*mm^2]
I_B = 1323;
b   = 10; % [mN*m*s/rad]
g = 9.81*1000; % [mm/s^2]

%% Step 1: Linearization
% [M's]*[qddot's] = [f's]

syms x theta x_dot theta_dot T_x %r_m l_r r_B r_G l_P r_P r_C m_B m_P I_P I_B b g

% M matrix [g*mm] or LHS of our non-linear equation
M = [-(m_B*r_B^2+m_B*r_C*r_B+I_B)/r_B, -(I_B*r_B+I_P*r_B+(m_B*r_B^3)+(m_B*r_B*r_C^2)+2*m_B*r_B^2*r_C+(m_P*r_B*r_G^2)+m_B*r_B*x)/r_B; 
    -(m_B*r_B^2+I_B)/r_B, -(m_B*r_B^3+m_B*r_C*r_B^2+I_B*r_B)/r_B];

% RHS function v(x, theta, x_dot, theta_dot, T_x)
fs = [(b*theta_dot)-(g*m_B*(sin(theta)*(r_B+r_C)+x*cos(theta)))+((T_x*l_P)/r_m)+(2*m_P*theta_dot*x_dot*x)-(g*m_P*r_G*sin(theta)); -m_B*r_B*x*theta_dot^2-g*m_B*r_B*sin(theta)];
% Multiply by the Inverse
qddot = M\fs;

dxdt = [x_dot;theta_dot;qddot(1);qddot(2)];

% Take Jacobian
A1 = jacobian(dxdt,[x,theta,x_dot,theta_dot]);
B1 = jacobian(dxdt, T_x);

% Substitute Values at Equilibrium Point
A2 = subs(A1,[x,theta,x_dot,theta_dot,T_x],[0,0,0,0,0]);
B2 = subs(B1,[x,theta,x_dot,theta_dot,T_x],[0,0,0,0,0]);

%% Step 2: State Space Modeling
A = double(A2);
B = double(B2);
C = eye(4);
D = zeros(4,1);
sys = ss(A,B,C,D)
%% Step 3: Simulation
% a)The ball is initially at rest on a level platform directly above the 
%   center of gravity of the platform and there is no torque input from 
%   the motor. Run this simulation for 1 [s].

init_cond = [0;0;0;0]; % In the order of [x,theta,x_dot,theta_dot]
t = 0:0.01:1;

% Plotting Output
u = zeros(size(t));
[y,t,x] = lsim(sys,u,t,init_cond);
figure('Name','Lab5: 6a','NumberTitle','off')
subplot(4,1,1)
plot(t,y)
title('3a) State Vector')
ylabel('x')
subplot(4,1,2)
plot(t,y)
ylabel('\theta')
subplot(4,1,3)
plot(t,y)
ylabel('$\dot{x}$','Interpreter','latex')
subplot(4,1,4)
plot(t,y)
ylabel('$\dot{\theta}$','Interpreter','latex')
xlabel('Time [s]')

% b)The ball is initially at rest on a level platform offset horizontally 
% from the center of gravity of the platform by 5 [cm]= 50mm and there is no 
% torque input from the motor. Run this simulation for 0.4 [s].

init_cond = [50;0;0;0]; % In the order of [x,theta,x_dot,theta_dot]
t = 0:0.01:0.4;
%output = sim('Lab6');

% Plotting Output
u = zeros(size(t));
[y,t,x] = lsim(sys,u,t,init_cond);
n = rank(obsv(A,C));

% Save state variables explicitly to aid in plotting
X = x(:,1); theta = x(:,2); x_dot = x(:,3); theta_dot = x(:,4);

figure('Name','Lab6: 3b','NumberTitle','off')
subplot(4,1,1)
plot(t,x(:,1))
title('3b) State Vector')
ylabel('x')
subplot(4,1,2)
plot(t,x(:,2))
ylabel('\theta')
subplot(4,1,3)
plot(t,x_dot)
ylabel('$\dot{x}$','Interpreter','latex')
subplot(4,1,4)
plot(t,x(:,4))
ylabel('$\dot{\theta}$','Interpreter','latex')
xlabel('Time [s]')

% (c) The ball is initially at rest on a platform inclined at 5◦ directly
% above the center of gravity of the platform and there is no torque input 
% from the motor. Run this simulation for 0.4 [s].

init_cond = [0;5;0;0]; % In the order of [x,theta,x_dot,theta_dot]
t = 0:0.01:0.4;
%output = sim('Lab6');

% Plotting Output
u = zeros(size(t));
[y,t,x] = lsim(sys,u,t,init_cond);

figure('Name','Lab6: 3c','NumberTitle','off')
subplot(4,1,1)
plot(t,x(:,1))
title('3c) State Vector')
ylabel('x')
subplot(4,1,2)
plot(t,x(:,2))
ylabel('\theta')
subplot(4,1,3)
plot(t,x_dot)
ylabel('$\dot{x}$','Interpreter','latex')
subplot(4,1,4)
plot(t,x(:,4))
ylabel('$\dot{\theta}$','Interpreter','latex')
xlabel('Time [s]')

% (d) The ball is initially at rest on a level platform directly above the 
% center of gravity of the platform and there is an impulse1 of 1 mNm · s 
% applied by the motor. Run this simulation for 0.4 [s].

init_cond = [0;0;0;0]; % In the order of [x,theta,x_dot,theta_dot]
t = 0:0.01:0.4;

% Plotting Output
%u = zeros(size(t))
result = sim('Lab6');

figure('Name','Lab6: 3d','NumberTitle','off')
subplot(4,1,1)
plot(result.tout,result.simout(:,1))
title('3d) State Vector')
ylabel('x')
subplot(4,1,2)
plot(result.tout,result.simout(:,2))
ylabel('\theta')
subplot(4,1,3)
plot(result.tout,result.simout(:,3))
ylabel('$\dot{x}$','Interpreter','latex')
subplot(4,1,4)
plot(result.tout,result.simout(:,4))
ylabel('$\dot{\theta}$','Interpreter','latex')
xlabel('Time [s]')

%% Step 4: Closed Loop Simulation
%Gain
gain = [-0.05 -0.02 -0.3 -0.2];
init_cond = [50;0;0;0]; % In the order of [x,theta,x_dot,theta_dot]
tspan = 20;

results = sim('Lab6');
figure('Name','Lab6: 4a','NumberTitle','off')
subplot(4,1,1)
plot(results.tout,results.sim2out(:,1))
title('4a) State Vector')
ylabel('x')
subplot(4,1,2)
plot(results.tout,results.sim2out(:,2))
ylabel('\theta')
subplot(4,1,3)
plot(results.tout,results.sim2out(:,3))
ylabel('$\dot{x}$','Interpreter','latex')
subplot(4,1,4)
plot(results.tout,results.sim2out(:,4))
ylabel('$\dot{\theta}$','Interpreter','latex')
xlabel('Time [s]')
